import 'package:json_annotation/json_annotation.dart';

part 'ingredient.g.dart';

/// commande de génération : flutter packages pub run build_runner build
@JsonSerializable(fieldRename: FieldRename.snake)
class Ingredient {
  final int id;
  final String name;
  final String picture;

  Ingredient(this.id, this.name, this.picture);

  factory Ingredient.fromJson(Map<String, dynamic> json) =>
      _$IngredientFromJson(json);  Map<String, dynamic> toJson() => _$IngredientToJson(this);
}