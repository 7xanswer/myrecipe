import 'package:json_annotation/json_annotation.dart';
import 'package:recipe/models/ingredient.dart';

part 'recipe_ingredient.g.dart';

/// commande de génération : flutter packages pub run build_runner build
@JsonSerializable(fieldRename: FieldRename.snake)
class RecipeIngredient {
  final int position;
  final List<Ingredient> alternatives;

  RecipeIngredient(this.position, this.alternatives);

  factory RecipeIngredient.fromJson(Map<String, dynamic> json) =>
      _$RecipeIngredientFromJson(json);  Map<String, dynamic> toJson() => _$RecipeIngredientToJson(this);
}