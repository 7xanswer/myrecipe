import 'package:json_annotation/json_annotation.dart';
import 'package:recipe/models/recipe_ingredient.dart';

part 'recipe.g.dart';

/// commande de génération : flutter packages pub run build_runner build
@JsonSerializable(fieldRename: FieldRename.snake)
class Recipe {
  final int id;
  final String name;
  final List<RecipeIngredient> ingredients;
  final String picture;


  Recipe(this.id, this.name, this.ingredients, this.picture);

  factory Recipe.fromJson(Map<String, dynamic> json) =>
      _$RecipeFromJson(json);  Map<String, dynamic> toJson() => _$RecipeToJson(this);
}