// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recipe.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Recipe _$RecipeFromJson(Map<String, dynamic> json) {
  return Recipe(
    json['id'] as int,
    json['name'] as String,
    (json['ingredients'] as List)
        ?.map((e) => e == null
            ? null
            : RecipeIngredient.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['picture'] as String,
  );
}

Map<String, dynamic> _$RecipeToJson(Recipe instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'ingredients': instance.ingredients,
      'picture': instance.picture,
    };
