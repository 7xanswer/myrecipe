import 'package:json_annotation/json_annotation.dart';
import 'package:recipe/models/ingredient.dart';
import 'package:recipe/models/recipe.dart';

part 'find_recipes.g.dart';

/// commande de génération : flutter packages pub run build_runner build --delete-conflicting-outputs
@JsonSerializable(fieldRename: FieldRename.snake)
class FindRecipes {
  final List<Recipe> recipes;

  FindRecipes(this.recipes);

  factory FindRecipes.fromJson(Map<String, dynamic> json) =>
      _$FindRecipesFromJson(json);  Map<String, dynamic> toJson() => _$FindRecipesToJson(this);
}