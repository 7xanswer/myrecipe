import 'package:json_annotation/json_annotation.dart';
import 'package:recipe/models/ingredient.dart';

part 'banned_ingredients.g.dart';

/// commande de génération : flutter packages pub run build_runner build --delete-conflicting-outputs
@JsonSerializable(fieldRename: FieldRename.snake)
class BannedIngredients {
  final List<Ingredient> banned;

  BannedIngredients(this.banned);

  factory BannedIngredients.fromJson(Map<String, dynamic> json) =>
      _$BannedIngredientsFromJson(json);  Map<String, dynamic> toJson() => _$BannedIngredientsToJson(this);
}