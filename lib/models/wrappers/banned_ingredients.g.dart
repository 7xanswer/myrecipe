// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'banned_ingredients.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BannedIngredients _$BannedIngredientsFromJson(Map<String, dynamic> json) {
  return BannedIngredients(
    (json['banned'] as List)
        ?.map((e) =>
    e == null ? null : Ingredient.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$BannedIngredientsToJson(BannedIngredients instance) =>
    <String, dynamic>{
      'banned': instance.banned,
    };