// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'find_recipes.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FindRecipes _$FindRecipesFromJson(Map<String, dynamic> json) {
  return FindRecipes(
    (json['recipes'] as List)
        ?.map((e) =>
    e == null ? null : Recipe.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$FindRecipesToJson(FindRecipes instance) =>
    <String, dynamic>{
      'recipes': instance.recipes,
    };