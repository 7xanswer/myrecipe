import 'package:recipe/pages/add_recipe_page.dart';
import 'package:recipe/pages/home_page.dart';
import 'package:recipe/pages/log_in_page.dart';
import 'package:recipe/pages/main_page.dart';
import 'package:recipe/pages/menu_page.dart';
import 'package:recipe/pages/profile_page.dart';
import 'package:recipe/pages/search_page.dart';
import 'package:recipe/pages/sign_up_page.dart';
import 'package:recipe/values/shared_preferences_keys.dart';
import 'package:recipe/values/strings.dart';
import 'package:flutter/material.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

void main() {
  runApp(MyApp());
}

class MyBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (context, child) {
        return ScrollConfiguration(
          behavior: MyBehavior(),
          child: child,
        );
      },
      title: Strings.APP_NAME,
      theme: ThemeData(
          primaryColor: Strings.PrimaryColor,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          fontFamily: 'Quicksand',
          bottomSheetTheme: BottomSheetThemeData(backgroundColor: Colors.white)
      ),
      home: FutureBuilder<StreamingSharedPreferences>(
        future: StreamingSharedPreferences.instance,
        builder: (ctx, snap){

          if(null == snap || !snap.hasData){
            return  Column(
              children: [
                Image.asset("assets/images/app_icon.png"),
                //Text("Initialisation..."),
              ],
            );
          }

          return PreferenceBuilder<String>(
              preference: snap.data.getString(SharedPreferencesKeys.USER_NICKNAME, defaultValue: ""),
              builder: (BuildContext context, String username) {
                if("" == username){
                  return HomePage();
                }else{
                  return MainPage();
                }
              }
          );
        },
      ),
      routes: {
        LogInPage.route: (context) => LogInPage(),
        SignUpPage.route: (context) => SignUpPage(),
        HomePage.route: (context) => HomePage(),
        MainPage.route: (context) => MainPage(),
        MenuPage.route: (context) => MenuPage(),
        AddRecipePage.route: (context) => AddRecipePage(),
        ProfilePage.route: (context) => ProfilePage(),
        SearchPage.route: (context) => SearchPage()
      },
    );
  }
}
