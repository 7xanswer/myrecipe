import 'package:flutter/material.dart';

class TopAmberCurve extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      child: Container(
        height: 300.0,
      ),
      painter: CurvePainter(),
    );
  }
}

class CurvePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Path path = Path();
    Paint paint = Paint();

    path = Path();
    path.lineTo(0, size.width * 1.3);
    path.quadraticBezierTo(
        size.width * 0.3, size.height, size.width, size.height);
    path.lineTo(size.width, 0);
    path.close();

    paint.shader = LinearGradient(
            colors: [
              Colors.amber,
              Colors.amberAccent,
            ],
        begin: FractionalOffset.topCenter,
        end: FractionalOffset.bottomCenter,
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp).createShader(Rect.fromCenter(width: size.width, height: size.height, center: Offset(0,360))
    );
    paint.color = Colors.amber;
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return oldDelegate != this;
  }
}
