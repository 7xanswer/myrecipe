import 'package:flutter/material.dart';

final title = TextStyle(
  fontFamily: 'Quicksand',
  fontWeight: FontWeight.w400,
  fontSize: 26.0,
  color: Colors.black,
);

final subtitle = TextStyle(
  fontFamily: 'Quicksand',
  fontWeight: FontWeight.w700,
  fontSize: 18.0,
  color: Colors.white,
);

final subtitleBlack = TextStyle(
  fontFamily: 'Quicksand',
  fontWeight: FontWeight.w700,
  fontSize: 18.0,
  color: Colors.black,
);

final textWhite = TextStyle(
  fontFamily: 'Quicksand',
  fontWeight: FontWeight.w400,
  fontSize: 14.0,
  color: Colors.white,
);

final textBlack = TextStyle(
  fontFamily: 'Quicksand',
  fontWeight: FontWeight.w400,
  fontSize: 14.0,
  color: Colors.black,
);
