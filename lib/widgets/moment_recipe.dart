import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:recipe/models/recipe.dart';
import 'package:recipe/pages/search_page.dart';
import 'package:recipe/services/abstract_http_service.dart';
import 'package:recipe/widgets/menu_moment.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

class MomentRecipe extends StatelessWidget {
  const MomentRecipe({
    Key key,
    @required this.widget,
    @required this.recipe,
    @required this.momentKey,
  }) : super(key: key);

  final MenuMoment widget;
  final Recipe recipe;
  final String momentKey;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: (recipe == null)
          ? NoRecipeCard(widget: widget)
          : RecipeCard(recipe: recipe, widget: widget, momentKey: momentKey),
    );
  }
}

class RecipeCard extends StatelessWidget {
  const RecipeCard({
    Key key,
    @required this.recipe,
    @required this.widget,
    @required this.momentKey,
  }) : super(key: key);

  final Recipe recipe;
  final MenuMoment widget;
  final String momentKey;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 8.0, bottom: 8.0, left: 16.0, right: 16.0,
      ),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.black12,
                blurRadius: 20,
                offset: Offset(0, 5)
            )
          ],
          borderRadius: BorderRadius.all(
            Radius.circular(30),
          ),
        ),
        child: Column(
          children: [
            Container(
                height: 196,
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                    ),
                    image: DecorationImage(
                        image: (recipe.picture != null)
                            ? NetworkImage(
                            AbstractHttpService.BASE_URL+"/img/rct/" +
                                recipe.picture)
                            : AssetImage("assets/img/Italian-food-illustration-tubikarts-2.png"),
                        fit: BoxFit.cover))),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 16.0),
                    child: Text(widget.moment),
                  ),
                  Expanded(child: Text(recipe.name)),
                  GestureDetector(
                    child: Icon(
                      Icons.delete_outline_rounded,
                      color: Colors.amber,
                    ),
                    onTap: () async {
                      var _sp = await StreamingSharedPreferences.instance;
                      _sp.setInt(momentKey, -1);
                    },
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class NoRecipeCard extends StatelessWidget {
  const NoRecipeCard({
    Key key,
    @required this.widget,
    this.loading,
  }) : super(key: key);

  final bool loading;
  final MenuMoment widget;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 8.0, bottom: 8.0, left: 16.0, right: 16.0,
      ),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.black12,
                blurRadius: 20,
                offset: Offset(0, 5)
            )
          ],
          borderRadius: BorderRadius.all(
            Radius.circular(30),
          ),
        ),
        child: Column(
          children: [
            Container(
              height: 30,
              width: double.infinity,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  ),
                  gradient: LinearGradient (
                      colors: [
                        Colors.amber,
                        Colors.amberAccent,
                      ],
                      begin: const FractionalOffset(0.0, 1),
                      end: const FractionalOffset(0, 0),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp
                  )
              ),
              child: Center(
                  child: loading ? CircularProgressIndicator() : Container()),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 16.0),
                    child: Text(widget.moment),
                  ),
                  Expanded(child: Text("No recipe chosen")),
                  GestureDetector(onTap: () async {
                    final recipeId = await Navigator.pushNamed(context, SearchPage.route);
                    if(null != recipeId) {
                      var momentKey = widget.date + "_" + widget.moment;
                      final sp = await StreamingSharedPreferences.instance;
                      sp.setInt(momentKey, recipeId);
                    }
                  }, child: Icon(Icons.search_rounded, color: Colors.amber)
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}