import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:recipe/models/ingredient.dart';
import 'package:recipe/services/abstract_http_service.dart';
import 'package:recipe/services/ingredients_service.dart';
import 'package:recipe/utilities/styles.dart';
import 'package:recipe/values/shared_preferences_keys.dart';
import 'package:recipe/values/strings.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';
import 'dart:convert' show utf8;

class IngredientCard extends StatelessWidget {
  final BuildContext context;
  final Ingredient ingredient;
  final bool banned;
  final IngredientsService ingredientsService = IngredientsService();
  final Function afterCloseModal;

  IngredientCard(
      {@required this.context,
        @required this.ingredient,
        this.banned: false,
        this.afterCloseModal});

  @override
  Widget build(BuildContext ctx) {
    return GestureDetector(
      onTap: () {
        _showModal();
      },
      child: Container(
        margin: const EdgeInsets.only(
          bottom: 10,
        ),
        decoration: BoxDecoration (
          color: Colors.amberAccent,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
          boxShadow: [
            BoxShadow(
            color: Colors.black12,
            blurRadius: 20,
            offset: Offset(0, 5)
            )
          ],
          border: Border.all(color: Colors.white, width: 2.0),
        ),

        child: Row(
          children: [
           Container(
             height: 50,
             width: 60,
             child: Icon(
               Icons.fastfood_rounded,
               color: Colors.white,
               size: 24.0,
             ),
             /*decoration: BoxDecoration(
                     shape: BoxShape.circle,
                     image: DecorationImage(
                     image: ingredient.picture ==null ? AssetImage("assets/img/Italian-food-illustration-tubikarts-2.png")
                         : NetworkImage(AbstractHttpService.BASE_URL + "/img/rct/" + ingredient.picture),
                         fit: BoxFit.cover
              )*/
              ),
            Text(
                ingredient.name,
                style: subtitle,
              ),
          ],
        ),
      ),
    );
  }

  Future<void> _showModal() async {
    final StreamingSharedPreferences sp =
    await StreamingSharedPreferences.instance;
    final Preference<List<String>> prefList = sp.getStringList(
        SharedPreferencesKeys.INGREDIENTS_LIST,
        defaultValue: List<String>());
    final List<String> list = prefList.getValue();
    final bool inMyList = list.contains(ingredient.id.toString());

    var a = await showModalBottomSheet(
        context: context,
        builder: (context) {
          return Column(
            children: [
              ListTile(
                title: Text(
                  ingredient.name,
                  style: title,
                ),
              ),
              banned
                  ? ListTile(
                title: Text(Strings.UNBAN_INGREDIENT),
                onTap: _unbanIngredient,
              )
                  : ListTile(
                title: Text(Strings.BAN_INGREDIENT),
                onTap: _banIngredient,
              ),
              inMyList
                  ? ListTile(
                title: Text(Strings.REMOVE_FROM_LIST),
                onTap: _addOrRemoveInList,
              )
                  : ListTile(
                title: Text(Strings.ADD_TO_LIST),
                onTap: _addOrRemoveInList,
              )
            ],
          );
        });

    if (null != afterCloseModal) {
      afterCloseModal();
    }
  }

  Future<void> _addOrRemoveInList() async {
    final StreamingSharedPreferences sp =
    await StreamingSharedPreferences.instance;
    final Preference<List<String>> prefList = sp.getStringList(
        SharedPreferencesKeys.INGREDIENTS_LIST,
        defaultValue: List<String>());
    final List<String> list = prefList.getValue();
    final bool inMyList = list.contains(ingredient.id.toString());

    if (!inMyList) {
      list.add(ingredient.id.toString());
    } else {
      list.remove(ingredient.id.toString());
    }

    sp.setStringList(SharedPreferencesKeys.INGREDIENTS_LIST, list);
    Navigator.pop(context);
  }

  void _banIngredient() {
    ingredientsService.ban(ingredient.id).then((value) {
      Navigator.pop(context, "OK");
    }).catchError((onError) {
      Navigator.pop(context, "KO");
    });
  }

  void _unbanIngredient() {
    ingredientsService.unban(ingredient.id).then((value) {
      Navigator.pop(context);
    }).catchError((onError) {
      Navigator.pop(context);
    });
  }
}