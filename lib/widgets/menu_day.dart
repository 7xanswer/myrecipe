import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:recipe/utilities/styles.dart';

import 'menu_moment.dart';

class MenuDay extends StatelessWidget {

  final DateTime date;
  MenuDay({@required this.date});

  @override
  Widget build(BuildContext context) {
    var key = date.day.toString() + "/" + date.month.toString();
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(
            top: 32, bottom: 8.0, left: 16.0, right: 16.0,
          ),
          child: Container (
            decoration: BoxDecoration (
              color: Colors.amberAccent,
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
              boxShadow: [
                BoxShadow(
                    color: Colors.black12,
                    blurRadius: 20,
                    offset: Offset(0, 5)
                )
              ],
              border: Border.all(color: Colors.white, width: 2.0),
            ),
            child: Row(
              children: [
                Container(
                  height: 50,
                  width: 60,
                  child: Icon(
                    Icons.calendar_today_rounded,
                    color: Colors.white,
                  ),
                ),
                Text(
                  key, style: subtitle,
                )
              ],
            ),
          ),
        ),
        MenuMoment(date: key, moment: "LUNCH"),
        MenuMoment(date: key, moment: "DINNER")
      ],
    );
  }
}