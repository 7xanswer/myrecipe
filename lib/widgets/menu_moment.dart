import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:recipe/models/recipe.dart';
import 'package:recipe/services/recipes_service.dart';
import 'package:recipe/widgets/moment_recipe.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

class MenuMoment extends StatefulWidget {

  final _recipesService = RecipesService();

  final String date;
  final String moment;

  MenuMoment({
    @required this.date,
    @required this.moment
  });

  @override
  _MenuMomentState createState() => _MenuMomentState();
}

class _MenuMomentState extends State<MenuMoment> {

  @override
  Widget build(BuildContext context) {

    return FutureBuilder(
      future: StreamingSharedPreferences.instance,
      builder: (ctx, spSnap){

        if(null == spSnap || !spSnap.hasData){
          return NoRecipeCard(widget: widget, loading: true);
        }

        final StreamingSharedPreferences sp = spSnap.data;
        var momentKey = widget.date + "_" + widget.moment;

        return PreferenceBuilder(
          preference: sp.getInt(momentKey, defaultValue: -1),
          builder: (BuildContext ctx, int recipeId){
            if(recipeId == null || recipeId == -1){
              return NoRecipeCard(widget: widget, loading: false);
            }
            return FutureBuilder(
              future: widget._recipesService.getOne(recipeId),
              builder: (ctx, snap) {

                if(null == snap || !snap.hasData){
                  return NoRecipeCard(widget: widget, loading: true);
                }
                final Recipe recipe = snap.data;
                return MomentRecipe(widget: widget, recipe: recipe, momentKey: momentKey);
              },
            );
          },
        );
      },
    );
  }
}