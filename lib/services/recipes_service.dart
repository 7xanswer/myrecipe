import 'dart:convert';

import 'package:recipe/models/ingredient.dart';
import 'package:recipe/models/wrappers/find_recipes.dart';
import 'package:recipe/services/abstract_http_service.dart';
import '../models/ingredient.dart';
import '../models/recipe.dart';
import '../models/recipe_ingredient.dart';


class RecipesService extends AbstractHttpService {
  static const DOMAIN_BASE_URL = '/rcp';

  Future<Recipe> getIdea() async {
    var response = await get(DOMAIN_BASE_URL + "/idea?u=ksa");
    return Future.value(Recipe.fromJson(jsonDecode(response.body.toString())));
  }

  Future<Recipe> getOne(final int recipeId) async {
    var response = await get(DOMAIN_BASE_URL + "/" + recipeId.toString());
    return Future.value(Recipe.fromJson(jsonDecode(response.body.toString())));
  }

  Future<FindRecipes> find(final String name) async {
    var response = await get(DOMAIN_BASE_URL + "/by-name?n=" + name);
    return Future.value(FindRecipes.fromJson(jsonDecode(response.body.toString())));

    List<Recipe> recipes = List();
    recipes.add(Recipe(1, "Recette 1", null, null));
    recipes.add(Recipe(2, "Recette 2", null, null));
    recipes.add(Recipe(3, "Recette 3", null, null));
    return Future.value(FindRecipes(recipes));
  }

  /// Service de creation de recette.
  Future<Recipe> create(final String pRecipeName, final List<String> pIngredients) async {

    final List<RecipeIngredient> ingredients = List<RecipeIngredient>();
    var position = 0;
    for(var i in pIngredients){
      List<Ingredient> alts = List<Ingredient>();
      alts.add(Ingredient(null, i, null));
      ingredients.add(RecipeIngredient(position, alts));
      position++;
    }

    var body = jsonEncode(Recipe(null, pRecipeName, ingredients, null));
    print(body);

    final response = await post(
        DOMAIN_BASE_URL,
        body
    );
    var json = jsonDecode(response.body);
    return Future.value(Recipe.fromJson(json));

  }

}