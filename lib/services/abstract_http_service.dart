import 'dart:convert';

import 'package:http/http.dart' as http;

abstract class AbstractHttpService {
  static const BASE_URL = 'http://kevinsamyn.ovh:8888/recipe';

  Future<http.Response> get(final String uri) async {
    var response = await http.get(BASE_URL + uri);
    return _handleResponse(response, uri);
  }

  Future<http.Response> delete(final String uri) async {
    var response = await http.delete(BASE_URL + uri);
    return _handleResponse(response, uri);
  }

  Future<http.Response> post(final String uri, final pBody) async {
    var response = await http.post(BASE_URL + uri,
        headers: {"Content-Type": "application/json", "Accept": "charset=UTF-8"}, body: pBody);
    return _handleResponse(response, uri);
  }

  http.Response _handleResponse(
      final http.Response response, final String uri) {
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      return response;
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed : ' + uri);
    }
  }
}
