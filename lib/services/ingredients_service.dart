import 'dart:convert';

import 'package:recipe/models/ingredient.dart';
import 'package:recipe/models/wrappers/banned_ingredients.dart';
import 'package:recipe/services/abstract_http_service.dart';
import '../models/ingredient.dart';
import '../models/recipe.dart';
import '../models/recipe_ingredient.dart';

class IngredientsService extends AbstractHttpService {
  static const DOMAIN_BASE_URL = '/igd';

  Future<Ingredient> getOne(final String ingredientId) async {
    var response = await get(DOMAIN_BASE_URL + "/by-id/" + ingredientId);
    return Future.value(Ingredient.fromJson(jsonDecode(response.body.toString())));
  }

  /// Service de bannissement d'un ingredient
  Future<bool> ban(final int id) async {
    await post(
        DOMAIN_BASE_URL + "/ban/" + id.toString()+"?u=ksa",
        null
    );
    return Future.value(true);
  }

  Future<bool> unban(final int id) async {
    var response = await delete(
        DOMAIN_BASE_URL + "/ban/" + id.toString()+"?u=ksa"
    );
    return Future.value(true);
  }

  Future<BannedIngredients> banned() async {
    var response = await get(DOMAIN_BASE_URL + "/banned?u=ksa");
    return Future.value(BannedIngredients.fromJson(jsonDecode(response.body.toString())));
  }

}