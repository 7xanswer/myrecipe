import 'dart:convert';

import 'package:recipe/models/user.dart';
import 'package:recipe/services/abstract_http_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../values/shared_preferences_keys.dart';
import '../values/shared_preferences_keys.dart';

class UsersService extends AbstractHttpService {
  static const DOMAIN_BASE_URL = '/usr';

  /// Service d'inscription
  Future<User> signUp(final String user) async {
    try {
      final response = await post(DOMAIN_BASE_URL + '/sign-up?u='+user, null);
      var json = jsonDecode(response.body);
      return User.fromJson(json);
    } catch(error) {
      return Future.value(new User(-1,'error'));
    }

  }
  /// Service de connexion
  Future<User> signIn(final String user) async {
    final response = await get(DOMAIN_BASE_URL + '/sign-in?u=' + user);
    var json = jsonDecode(response.body);
    print(json.toString());
    return Future.value(User.fromJson(json));
  }

  /// Service de déconnexion
  Future<bool> signOut() async {
    return Future.value(true);
  }
}
