import 'dart:ui';

/// Déclaration de l'ensemble des textes utilisés dans l'application
class Strings {
  static const String APP_NAME = 'Recipe';
  static const String APP_BASELINE = 'Discover and share recipes';
  static const String SIGN_UP_TITLE = 'Sign up';
  static const String SIGN_UP_SUBMIT_LABEL = 'Let\'s go';
  static const String SIGN_IN_TITLE = 'Sign in';
  static const String SIGN_IN_SUBMIT_LABEL = 'Sign in';

  static const PrimaryColor = const Color(0xFFF25C54);

  static const String ADD_TO_MENU = "Add to menu";
  static const String MENU_TITLE = 'Menu';
  static const String BAN_INGREDIENT = "Ban the ingredient from my diet";
  static const String UNBAN_INGREDIENT = "Re-authorize";
  static const String PROFILE_TITLE = "User profile";
  static const String BANNED_INGREDIENTS = "Banned ingredients";
  static const String ADD_TO_LIST = "Add to my shopping list";
  static const String REMOVE_FROM_LIST = "Remove from my shopping list";
  static const String INGREDIENTS_LIST = "Shopping list";
  static const String SEARCH_TITLE = "Find a recipe";
}