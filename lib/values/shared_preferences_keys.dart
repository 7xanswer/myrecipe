/// Liste des clés définissables dans le préférences partagées
class SharedPreferencesKeys {
  static const String USER_NICKNAME = 'user_nickname';
  static const String INGREDIENTS_LIST = 'igd_list';
}