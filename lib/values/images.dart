import 'package:flutter/widgets.dart';

/// Contient la définition de l'ensemble des images utilisées dans l'application
/// Attention a bien déclarer les ressources dans le fichier pubspec.yaml
class Images {
  static var APP_ICON = Image.asset(
    "assets/img/app_icon.png",
    width: 128,
  );
}