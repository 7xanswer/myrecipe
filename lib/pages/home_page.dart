import 'package:flutter/material.dart';
import 'package:recipe/pages/log_in_page.dart';
import 'package:recipe/pages/sign_up_page.dart';
import 'package:recipe/utilities/styles.dart';
import 'package:recipe/utilities/top_white_curve.dart';
import 'package:recipe/widgets/show_bottom.dart';
import 'package:recipe/widgets/show_top.dart';

class HomePage extends StatefulWidget {
  static const String route = 'home';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int delayAmount = 500;
  final int _numPages = 3;
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;

  List<Widget> _buildPageIndicator() {
    List<Widget> list = [];
    for (int i = 0; i < _numPages; i++) {
      list.add(i == _currentPage ? _indicator(true) : _indicator(false));
    }
    return list;
  }

  Widget _indicator(bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 260),
      margin: EdgeInsets.symmetric(horizontal: 8.0),
      height: 3.0,
      width: 24.0,
      decoration: BoxDecoration(
        color: isActive ? Colors.white : Colors.black,
        borderRadius: BorderRadius.all(Radius.circular(3)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          title: Text('MYRECIPE',
              style: TextStyle(
                  fontSize: 18.0,
                  letterSpacing: 4.0,
                  fontWeight: FontWeight.w400,
                  color: Colors.black)),
          centerTitle: true,
        ),
      body: Stack(
        children: <Widget>[
          ShowTop( child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient (
                colors: [
                  Colors.amber,
                  Colors.amberAccent,
                ],
                begin: const FractionalOffset(0.0, 0.6),
                end: const FractionalOffset(0, 0),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp
              )
            ),
          ), delay: delayAmount + 400,
          ),
        TopWhiteCurve(),
        Container(
          child: Padding(
            padding: EdgeInsets.only(bottom: 15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Container(
                  height: 600.0,
                  child: PageView(
                    physics: ClampingScrollPhysics(),
                    controller: _pageController,
                    onPageChanged: (int page) {
                      setState(() {
                        _currentPage = page;
                      });
                    },
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(40.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Image(
                                image: AssetImage(
                                  'assets/img/taxi-mushroom-picker.png',
                                ),
                                height: 300.0,
                                width: 300.0,
                              ),
                            ),
                            SizedBox(height: 30.0),
                            ShowTop(
                              child: Column(
                                children: <Widget>[
                                  Text("Welcome to MyRecipe\nyour food assistant", style: title,
                                  )
                                ],
                              ),
                              delay: delayAmount,
                            ),
                            SizedBox(height: 15.0),
                            ShowTop(
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    'A simple and intuitive interface to guide you on a daily basis in the preparation of your meals.',
                                    style: subtitle,
                                  ),
                                ],
                              ),
                              delay: delayAmount + 400,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(40.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Image(
                                image: AssetImage(
                                  'assets/img/taxi-593.png',
                                ),
                                height: 300.0,
                                width: 300.0,
                              ),
                            ),
                            SizedBox(height: 30.0),
                            ShowTop(
                              child: Column(
                                children: <Widget>[
                                  Text("Fresh and delicious food\non the menu", style: title,
                                  )
                                ],
                              ),
                              delay: delayAmount,
                            ),
                            SizedBox(height: 15.0),
                            ShowTop(
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    'Many recipes every day, plan your shopping and enjoy the taste of homemade.',
                                    style: subtitle,
                                  ),
                                ],
                              ),
                              delay: delayAmount + 400,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(40.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Image(
                                image: AssetImage(
                                  'assets/img/taxi-606.png',
                                ),
                                height: 300.0,
                                width: 300.0,
                              ),
                            ),
                            SizedBox(height: 30.0),
                            ShowTop(
                              child: Column(
                                children: <Widget>[
                                  Text("Healthy habits\nto another level", style: title,
                                  )
                                ],
                              ),
                              delay: delayAmount,
                            ),
                            SizedBox(height: 15.0),
                            ShowTop(
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    'Organize your meals according to your preferences, find alternatives quickly and easily, MyRecipe does it for you',
                                    style: subtitle,
                                  ),
                                ],
                              ),
                              delay: delayAmount + 400,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: _buildPageIndicator(),
                ),
                _currentPage != _numPages - 1
                    ? Expanded(
                  child: Align(
                    alignment: FractionalOffset.bottomRight,
                    child: FlatButton(
                      onPressed: () {
                        _pageController.nextPage(
                          duration: Duration(milliseconds: 600),
                          curve: Curves.ease,
                        );
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          SizedBox(width: 10.0),
                          Icon(
                            Icons.arrow_forward_rounded,
                            color: Colors.white,
                            size: 30.0,
                          ),
                        ],
                      ),
                    ),
                  ),
                )
                    : Text(''),
              ],
            ),
          ),
        ),
      ]
      ),
      bottomSheet: _currentPage == _numPages - 1 ?
      Container(
          height: 160.0,
          width: double.infinity,
          color: Colors.amber,
            child: Center(
              child: ShowTop(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30)
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.amberAccent.withOpacity(0.6),
                        spreadRadius: 8,
                        blurRadius: 13,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                child : FlatButton.icon(
                  padding: EdgeInsets.only(left: 30, right: 30, top: 15, bottom: 15),
                  color: Colors.amberAccent,
                  onPressed: () {
                    Navigator.pushNamed(context, LogInPage.route);
                  },
                  icon: Text(
                    "Get started",style: TextStyle(
                    fontFamily: 'Quicksand',
                    fontWeight: FontWeight.w700,
                    fontSize: 18.0,
                    color: Colors.white),
                  ),
                  label: Icon(Icons.login_rounded,
                    color: Colors.white,
                    size: 24.0,
                  ),
                  shape: RoundedRectangleBorder(
                    side: BorderSide(color: Colors.white,width: 2, style: BorderStyle.solid),
                      borderRadius: BorderRadius.circular(30)
                  ),
                ),
              ),delay: delayAmount + 600,
              )
            )
      ): Text('')
    );
  }
}