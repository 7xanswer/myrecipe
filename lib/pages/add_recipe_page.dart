import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:recipe/services/recipes_service.dart';

class AddRecipePage extends StatefulWidget {
  static const String route = 'add-recipe';

  var _recipesService = RecipesService();

  @override
  _AddRecipePageState createState() => _AddRecipePageState();
}

class _AddRecipePageState extends State<AddRecipePage> {
  // Controller du champ "nom de la recette". Il nous permet de récupérer la valeur.
  var _controller = TextEditingController();
  var _ingredientNameCtrl = TextEditingController();

  // Liste des ingredients de la recette a créer.
  // Elle est modifiée dans un setState ce qui permet de redessiner les boutons.
  final List<String> ingredients = List<String>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ajout de recette"),
      ),
      body: Column(
        children: [
          TextField(
            controller: _controller,
            textAlign: TextAlign.center,
            decoration: InputDecoration(hintText: 'Nom de la recette'),
          ),
          RaisedButton(
              child: Text('Ajouter un ingredient'),
              onPressed: () {
                _showModal();
              }),
          Expanded(
              child: ListView.builder(
                  itemCount: ingredients.length,
                  itemBuilder: (context, position) {
                    return GestureDetector(
                      onTap: (){
                        setState(() {
                          ingredients.remove(ingredients[position]);
                        });
                      },
                      child: Card(
                        color: Colors.red,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(ingredients[position]),
                        ),
                      ),
                    );
                  })
          ),
        ],
      ),
      // Bouton en base à droite de l'écran permettant de valider la création.
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          final String recipeName = _controller.text;
          print("try create recipe : " +
              recipeName +
              ":" +
              ingredients.toString());

          widget._recipesService.create(recipeName, ingredients);
        },
      ),
    );
  }

  /// Methode chargée de construire les boutons représentant les ingredients de la recette.
  List<Widget> buildIngredientsButtons() {
    List<Widget> results = List();
    for (var ingredientName in ingredients) {
      results.add(SizedBox(
        height: 50,
        child: RaisedButton(
            child: Text(ingredientName),
            onPressed: () {
              setState(() {
                ingredients.remove(ingredientName);
              });
            }),
      ));
    }
    return results;
  }

  /// Méthode ayant pour but d'afficher une modale permettant la saisie du nom d'un aliment
  void _showModal() async {
    await showDialog(
        context: context,
        child: AlertDialog(
          content: TextField(
            controller: _ingredientNameCtrl,
            decoration: InputDecoration(hintText: 'Nom de l\'ingredient'),
          ),
          actions: [
            RaisedButton(
                child: Text('Annuler'),
                onPressed: () {
                  Navigator.pop(context);
                }),
            RaisedButton(
                child: Text('Valider'),
                onPressed: () {
                  setState(() {
                    // On ajout le nom de l'ingredient dans la liste
                    ingredients.add(_ingredientNameCtrl.text);
                    // On ferme la modale
                    Navigator.pop(context);
                    // On efface le nom de l'ingredient dans le champ text
                    _ingredientNameCtrl.clear();
                  });
                })
          ],
        ));
  }
}