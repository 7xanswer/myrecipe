import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:recipe/values/strings.dart';
import 'package:recipe/widgets/menu_day.dart';

import 'main_page.dart';

class MenuPage extends StatelessWidget {
  static const String route = 'menu';

  @override
  Widget build(BuildContext context) {

    DateTime today = DateTime.now();

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
          onPressed: () {
            Navigator.pushNamed(context, MainPage.route);
          },
          icon: Icon(Icons.arrow_back_rounded),
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text('MYRECIPE',
            style: TextStyle(
                fontSize: 18.0,
                letterSpacing: 4.0,
                fontWeight: FontWeight.w400,
                color: Colors.black)),
        centerTitle: true,
      ),
      body: ListView.builder(itemCount: 5, itemBuilder: (ctx, position) {
        today = today.add(Duration(days: 1));
        return MenuDay(date: today);
      }),
    );
  }
}