import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:recipe/models/recipe.dart';
import 'package:recipe/pages/menu_page.dart';
import 'package:recipe/pages/profile_page.dart';
import 'package:recipe/services/abstract_http_service.dart';
import 'package:recipe/services/recipes_service.dart';
import 'package:recipe/utilities/styles.dart';
import 'package:recipe/utilities/top_white_curve.dart';
import 'package:recipe/values/strings.dart';
import 'package:recipe/widgets/ingredient_card.dart';
import 'package:recipe/widgets/show_bottom.dart';
import 'package:recipe/widgets/show_top.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';
import 'package:intl/intl.dart';


class MainPage extends StatefulWidget {
  static const String route = 'main';
  var _recipesService = RecipesService();

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  Future<Recipe> recipe;

  @override
  void initState() {
    super.initState();
    getRecipe();
  }

  bool viewVisible = false ;

  void showWidget(){
    setState(() {
      viewVisible = true ;
    });
  }

  void hideWidget(){
    setState(() {
      viewVisible = false ;
    });
  }

  _getTime() {
    final DateTime now = DateTime.now();
    final DateFormat format = DateFormat('d MMMM, y');
    final String output = format.format(now);
    return output; // something like 2013-04-20
  }

  _showDaySelectionModal(int recipeId) {
    DateTime today = DateTime.now();
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  "DAY",
                  style: TextStyle(fontSize: 24),
                ),
              ),
              Expanded(
                child: ListView.builder(
                    itemCount: 5,
                    itemBuilder: (ctx, position) {
                      today = today.add(Duration(days: 1));
                      final String date =
                          today.day.toString() + "/" + today.month.toString();
                      return ListTile(
                          title: Text(date),
                          leading: Icon(Icons.calendar_today_rounded),
                          onTap: () {
                            _showMomentSelectionModal(date, recipeId);
                          });
                    }),
              )
            ],
          );
        });
  }

  _showMomentSelectionModal(String date, int recipeId) {
    Navigator.pop(context);
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  date,
                  style: TextStyle(fontSize: 24),
                ),
              ),
              ListTile(
                title: Text("Lunch"),
                leading: Icon(Icons.calendar_today_rounded),
                onTap: () async {
                  final sp = await StreamingSharedPreferences.instance;
                  sp.setInt(date + "_LUNCH", recipeId);
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: Text("Dinner"),
                leading: Icon(Icons.calendar_today_rounded),
                onTap: () async {
                  final sp = await StreamingSharedPreferences.instance;
                  sp.setInt(date + "_DINNER", recipeId);
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  Future<void> getRecipe() async {
    setState(() {
      recipe = Future.value(null);
    });
    await Future.delayed(const Duration(seconds: 1), () {});
    setState(() {
      recipe = widget._recipesService.getIdea();
    });
  }

  int delayAmount = 500;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Navigator.pushNamed(context, MenuPage.route);
            },
            icon: Icon(Icons.schedule_rounded),
            color: Colors.black,
          ),
          backgroundColor: Colors.white,
          elevation: 0.0,
          title: Text('MYRECIPE',
              style: TextStyle(
                  fontSize: 18.0,
                  letterSpacing: 4.0,
                  fontWeight: FontWeight.w400,
                  color: Colors.black)),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.account_circle_outlined),
              onPressed: () {
                Navigator.pushNamed(context, ProfilePage.route);
              },
              color: Colors.black,
            )
          ],
        ),
        backgroundColor: Colors.white,
        floatingActionButton: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30)),
          boxShadow: [
            BoxShadow(
                color: Colors.black12,
                blurRadius: 20,
                offset: Offset(0, 5)
            )
          ],
        ),
        child: FlatButton.icon(
          padding: EdgeInsets.only(
              left: 30, right: 30, top: 15, bottom: 15),
          color: Colors.amberAccent,
          onPressed: () {
            getRecipe();
          },
          label: Text(
            "Refresh",
            style: TextStyle(
                fontFamily: 'Quicksand',
                fontWeight: FontWeight.w700,
                fontSize: 18.0,
                color: Colors.white),
          ),
          icon: Icon(
            Icons.refresh_rounded,
            color: Colors.white,
            size: 24.0,
          ),
          shape: RoundedRectangleBorder(
              side: BorderSide(
                  color: Colors.white,
                  width: 2,
                  style: BorderStyle.solid),
              borderRadius: BorderRadius.circular(30)),
        ),
      ),
      body: Center(
        child: FutureBuilder(
          future: recipe,
          builder: (ctx, snap) {
            if (snap == null || !snap.hasData) {
              return new CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(Colors.amber),
              );
            }
            final Recipe recipe = snap.data;
            return Stack(
              children: [
                ShowTop( child: Container(
                  height: MediaQuery.of(context).size.height - 82.0,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      gradient: LinearGradient (
                          colors: [
                            Colors.amber,
                            Colors.amberAccent,
                          ],
                          begin: const FractionalOffset(0.0, 0.6),
                          end: const FractionalOffset(0, 0),
                          stops: [0.0, 1.0],
                          tileMode: TileMode.clamp
                      )
                  ),
                ), delay: delayAmount + 400,
                ),
                TopWhiteCurve(),
                Container(
                  width: double.infinity,
                  height: 160,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          ShowBottom(
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: 10),
                              child: Text(
                                "Today " + _getTime(),
                                style: title,
                              ),
                            ), delay: delayAmount,
                          ),
                          ShowBottom(
                            child: Text(
                              "A delicious dish to cook now with\n" + recipe.ingredients.length.toString() + " ingredients needed",
                              style: textBlack,
                              textAlign: TextAlign.center,
                            ), delay: delayAmount + 400,
                          )
                        ],
                      ),
                ),
                Positioned(
                    top: 125,
                    left: (MediaQuery.of(context).size.width / 2) - 125.0,
                    child: ShowTop(
                      child: Container(
                        child: Hero(
                          tag: widget,
                          child: Container(
                            decoration: BoxDecoration(
                              //shape: BoxShape.circle,
                              image: DecorationImage(
                                image: recipe.picture ==null ? AssetImage("assets/img/asian_food_plate.png") : NetworkImage(AbstractHttpService.BASE_URL + "/img/rct/" + recipe.picture),
                              )
                            ),
                            height: 250.0,
                            width: 250.0,
                          )
                        )
                      ), delay: delayAmount + 600,
                    )
                ),
                SizedBox(height: 20.0),
                Positioned(
                    top: 275.0,
                    left: 25.0,
                    right: 25.0,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          ShowTop(child:Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(
                                Radius.circular(30),
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.white.withOpacity(0.6),
                                  blurRadius: 13,
                                  offset: Offset(
                                      0, -13),
                                ),
                                BoxShadow(
                                  color: Colors.black12,
                                  blurRadius: 20,
                                  offset: Offset(
                                      0, 5),
                                ),
                              ],
                              border: Border.all(color: Colors.amberAccent, width: 2.0),
                            ),
                            height: 350,
                            width: double.infinity,
                            child: Padding(
                              padding: EdgeInsets.all(30),
                              child: Column (
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  ShowTop( child: Text(
                                    recipe.name,
                                    style: title,
                                  ), delay: delayAmount + 1000,
                                  ),
                                  SizedBox(height: 15.0),
                                  Text(
                                    "Find the ingredients you need to make this delicious idea for your next meal",
                                    style: textBlack,
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(
                                        top: 20,
                                        bottom: 20,
                                      ),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(30),
                                        ),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.black12,
                                              blurRadius: 20,
                                              offset: Offset(0, 5))
                                        ],
                                      ),
                                      child: FlatButton.icon(
                                        padding: EdgeInsets.only(
                                            left: 30, right: 30, top: 15, bottom: 15),
                                        color: Colors.amberAccent,
                                        onPressed: () {
                                          showWidget();
                                        },
                                        icon: Text(
                                          "View",
                                          style: TextStyle(
                                              fontFamily: 'Quicksand',
                                              fontWeight: FontWeight.w700,
                                              fontSize: 18.0,
                                              color: Colors.white),
                                        ),
                                        label: Icon(
                                          Icons.fastfood_rounded,
                                          color: Colors.white,
                                          size: 24.0,
                                        ),
                                        shape: RoundedRectangleBorder(
                                            side: BorderSide(
                                                color: Colors.white,
                                                width: 2,
                                                style: BorderStyle.solid),
                                            borderRadius: BorderRadius.circular(30)),
                                      ),
                                    ),
                                  ),
                                  Text(
                                    "Plan this dish for the next few days ?",
                                    style: textBlack,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                      top: 20,
                                    ),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(30),
                                        ),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.black12,
                                              blurRadius: 20,
                                              offset: Offset(0, 5))
                                        ],
                                      ),
                                      child: FlatButton.icon(
                                        padding: EdgeInsets.only(
                                            left: 30, right: 30, top: 15, bottom: 15),
                                        color: Colors.amberAccent,
                                        onPressed: () {
                                          _showDaySelectionModal(recipe.id);
                                        },
                                        icon: Text(
                                          "Schedule",
                                          style: TextStyle(
                                              fontFamily: 'Quicksand',
                                              fontWeight: FontWeight.w700,
                                              fontSize: 18.0,
                                              color: Colors.white),
                                        ),
                                        label: Icon(
                                          Icons.schedule_send,
                                          color: Colors.white,
                                          size: 24.0,
                                        ),
                                        shape: RoundedRectangleBorder(
                                            side: BorderSide(
                                                color: Colors.white,
                                                width: 2,
                                                style: BorderStyle.solid),
                                            borderRadius: BorderRadius.circular(30)),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ), delay: delayAmount,),
                        ],
                    )
                ),
                Visibility(
                  maintainSize: true,
                    maintainAnimation: true,
                    maintainState: true,
                    visible: viewVisible,
                      child: Container(
                        padding: const EdgeInsets.all(30),
                        color: Colors.white,
                        height: double.infinity,
                        width: double.infinity,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "You need " + recipe.ingredients.length.toString() + " ingredients to cook " + recipe.name,
                                style: title,
                              ),
                              SizedBox(height: 15.0),
                              GestureDetector(
                                onTap: () { hideWidget(); },
                                child: Text(
                                    "Back to recipes",
                                  style: textBlack,
                                ),
                              ),
                              SizedBox(height: 30.0),
                              Container (
                                height: 400,
                                width: double.infinity,
                                child: ListView.builder(
                                  itemCount: recipe.ingredients.length,
                                  itemBuilder: (ctx, position) {
                                    return Container (
                                        child:
                                        IngredientCard(context: context, ingredient: recipe.ingredients[position].alternatives[0],
                                          afterCloseModal: (){
                                            //getRecipe();
                                          },)
                                    );
                                  }

                                ),
                              )
                            ],
                          ),
                          /*child: Container(
                              padding: const EdgeInsets.all(0),
                              child: ListView.builder(
                                  itemCount: recipe.ingredients.length,
                                  itemBuilder: (ctx, position) {
                                    return Container (
                                      child:
                                        IngredientCard(context: context, ingredient: recipe.ingredients[position].alternatives[0],
                                          afterCloseModal: (){
                                          hideWidget();
                                          //getRecipe();
                                          },)
                                    );
                                  })
                          )*/
                ),
                )
              ],
            );
          }
        ),
      )
    );
  }
}
