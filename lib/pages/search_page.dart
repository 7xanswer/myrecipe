import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:recipe/models/recipe.dart';
import 'package:recipe/models/wrappers/find_recipes.dart';
import 'package:recipe/services/recipes_service.dart';
import 'package:recipe/utilities/styles.dart';
import 'package:recipe/values/strings.dart';

import 'main_page.dart';

class SearchPage extends StatefulWidget {
  static const String route = 'search';
  final _controller = TextEditingController();
  final _recipesService = RecipesService();
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  var search = false;
  Future<FindRecipes> _future;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
          onPressed: () {
            Navigator.pushNamed(context, MainPage.route);
          },
          icon: Icon(Icons.arrow_back_rounded),
          color: Colors.black,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Text('MYRECIPE',
            style: TextStyle(
                fontSize: 18.0,
                letterSpacing: 4.0,
                fontWeight: FontWeight.w400,
                color: Colors.black)),
        centerTitle: true,
      ),
      body: FutureBuilder(
        future: _future,
        builder: (context, snap) {

          if(!search){
            return Center(
                child: Text("Search for a recipe by name")
            );
          }

          if(snap == null || !snap.hasData){
            return Center(child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.amber),
            ));
          }

          FindRecipes findRecipes = snap.data;
          if(findRecipes.recipes.isEmpty){
            return Center(child: Text("No matching recipe"));
          }

          return ListView.builder(
              itemCount: findRecipes.recipes.length,
              itemBuilder: (context, position) {
                return GestureDetector(
                  onTap: (){
                    var recipe = findRecipes.recipes[position];
                    Navigator.pop(context, recipe.id);
                  },
                  child: ListTile(
                    title: Container(
                      margin: const EdgeInsets.only(
                        bottom: 10,
                      ),
                      decoration: BoxDecoration (
                        color: Colors.amber,
                        borderRadius: BorderRadius.all(
                          Radius.circular(10),
                        ),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black12,
                              blurRadius: 20,
                              offset: Offset(0, 5)
                          )
                        ],
                        border: Border.all(color: Colors.white, width: 2.0),
                      ),
                    child: Row(
                        children: [
                          Container(
                          height: 50,
                          width: 60,
                          child: Icon(
                            Icons.restaurant_menu_rounded,
                            color: Colors.white,
                            size: 24.0,
                          ),
                        ),
                          Text(
                              findRecipes.recipes[position].name,
                            style: subtitle
                          ),
                    ]),
                  ),
                )
                );
              });
        },
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(
          top: 0.0, bottom: 0.0, left: 16.0, right: 16.0,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(30),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black12,
                        blurRadius: 20,
                        offset: Offset(0, 5))
                  ]),
              child: TextField(
                controller: widget._controller,
                onChanged: (currentValue) {
                  _updateRecipes();
                },
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.amber, width: 2.0),
                    borderRadius: BorderRadius.circular(30),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.amberAccent,
                        width: 2.0),
                    borderRadius: BorderRadius.circular(30),
                  ),
                  hintText: "Recipe name",
                  fillColor: Colors.white,
                  contentPadding: EdgeInsets.all(18),
                  hintStyle: TextStyle(color: Colors.grey),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  void _updateRecipes() {
    var currentValue = widget._controller.text;
    setState(() {
      search = true;
      _future = widget._recipesService.find(currentValue);
    });
  }
}