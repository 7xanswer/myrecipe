import 'package:recipe/pages/sign_up_page.dart';
import 'package:flutter/material.dart';
import 'package:recipe/services/users_service.dart';
import 'package:recipe/utilities/styles.dart';
import 'package:flutter/widgets.dart';
import 'package:recipe/utilities/top_amber_curve.dart';
import 'package:recipe/widgets/show_bottom.dart';
import 'package:recipe/widgets/show_top.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';
import '../models/user.dart';
import '../values/shared_preferences_keys.dart';
import 'home_page.dart';
import 'main_page.dart';

class LogInPage extends StatelessWidget {
  static const String route = 'log_in';

  int delayAmount = 500;
  var _usersService = UsersService();
  var _nicknameCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
          onPressed: () {
            Navigator.pushNamed(context, HomePage.route);
          },
          icon: Icon(Icons.arrow_back_rounded),
          color: Colors.white,
        ),
        backgroundColor: Colors.amber,
        elevation: 0.0,
        title: Text('MYRECIPE',
            style: TextStyle(
                fontSize: 18.0,
                letterSpacing: 4.0,
                fontWeight: FontWeight.w400,
                color: Colors.white)),
        centerTitle: true,
      ),
      body: Stack(
        children: <Widget>[
          Container(
            color: Colors.white,
          ),
          TopAmberCurve(),
          Container(
            width: double.infinity,
            height: double.infinity,
            child: Padding(
              padding: EdgeInsets.only(bottom: 15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  SingleChildScrollView(
                    child: (Padding(
                      padding: EdgeInsets.all(40.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(height: 30.0),
                          ShowBottom(
                            child: Column(
                              children: <Widget>[
                                Text(
                                  "Log in to MyRecipe",
                                  style: title,
                                )
                              ],
                            ),
                            delay: delayAmount,
                          ),
                          SizedBox(height: 15.0),
                          ShowBottom(
                            child: Column(
                              children: <Widget>[
                                Text(
                                  'Back to discover a range of culinary possibilities.',
                                  style: subtitle,
                                ),
                              ],
                            ),
                            delay: delayAmount + 400,
                          ),
                          SizedBox(height: 15.0),
                          Column(
                            children: <Widget>[
                              Text(
                                "Don't have an account ?",
                                style: textBlack,
                              ),
                            ],
                          ),
                          SizedBox(height: 10.0),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(30),
                                  topRight: Radius.circular(30),
                                  bottomLeft: Radius.circular(30),
                                  bottomRight: Radius.circular(30)),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.amberAccent.withOpacity(0.6),
                                  spreadRadius: 8,
                                  blurRadius: 13,
                                  offset: Offset(0, 3), // changes position of shadow
                                ),
                              ],
                            ),
                            child: FlatButton.icon(
                              padding: EdgeInsets.only(
                                  left: 30, right: 30, top: 15, bottom: 15),
                              color: Colors.amberAccent,
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SignUpPage()),
                                );
                              },
                              icon: Text(
                                "Register",
                                style: TextStyle(
                                    fontFamily: 'Quicksand',
                                    fontWeight: FontWeight.w700,
                                    fontSize: 18.0,
                                    color: Colors.white),
                              ),
                              label: Icon(
                                Icons.how_to_reg_rounded,
                                color: Colors.white,
                                size: 24.0,
                              ),
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                      color: Colors.white,
                                      width: 2,
                                      style: BorderStyle.solid),
                                  borderRadius: BorderRadius.circular(30)),
                            ),
                          )
                        ],
                      ),
                    )),
                  )
                ],
              ),
            ),
          ),
          Container(
            child: Align(
              child: Container(
                  padding: EdgeInsets.only(right: 40, left: 40, bottom: 30),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: Text(
                            "Your amazing username",
                            style: textBlack,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 20),
                          decoration: BoxDecoration(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.circular(30),
                          ),
                          child: Column(
                            children: <Widget>[
                              Container(
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(30),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.black12,
                                          blurRadius: 20,
                                          offset: Offset(0, 5))
                                    ]),
                                child: TextField(
                                  controller: _nicknameCtrl,
                                  decoration: InputDecoration(
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.amber, width: 2.0),
                                      borderRadius: BorderRadius.circular(30),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.amberAccent,
                                          width: 2.0),
                                      borderRadius: BorderRadius.circular(30),
                                    ),
                                    hintText: "Username",
                                    fillColor: Colors.white,
                                    contentPadding: EdgeInsets.all(18),
                                    hintStyle: TextStyle(color: Colors.grey),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        ShowTop(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(30),
                                  topRight: Radius.circular(30),
                                  bottomLeft: Radius.circular(30),
                                  bottomRight: Radius.circular(30)),
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black12,
                                    blurRadius: 20,
                                    offset: Offset(0, 5))
                              ],
                            ),
                            child: FlatButton.icon(
                              padding: EdgeInsets.only(
                                  left: 30, right: 30, top: 15, bottom: 15),
                              color: Colors.amberAccent,
                              onPressed: () {
                                var nickname = _nicknameCtrl.text;
                                print(nickname);_usersService.signIn(nickname).then((User user) async {
                                  final preferences = await StreamingSharedPreferences.instance;
                                  Preference<String> username = preferences.getString(SharedPreferencesKeys.USER_NICKNAME, defaultValue: "");
                                  username.setValue(user.name);
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => MainPage()),
                                  );
                                });
                              },
                              icon: Text(
                                "Log in",
                                style: TextStyle(
                                    fontFamily: 'Quicksand',
                                    fontWeight: FontWeight.w700,
                                    fontSize: 18.0,
                                    color: Colors.white),
                              ),
                              label: Icon(
                                Icons.login_rounded,
                                color: Colors.white,
                                size: 24.0,
                              ),
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                      color: Colors.white,
                                      width: 2,
                                      style: BorderStyle.solid),
                                  borderRadius: BorderRadius.circular(30)),
                            ),
                          ),
                          delay: delayAmount + 600,
                        ),
                      ])),
            ),
          ),
        ],
      ),
    );
  }
}
