import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:recipe/models/ingredient.dart';
import 'package:recipe/models/wrappers/banned_ingredients.dart';
import 'package:recipe/services/ingredients_service.dart';
import 'package:recipe/utilities/styles.dart';
import 'package:recipe/utilities/top_amber_curve.dart';
import 'package:recipe/utilities/top_white_curve.dart';
import 'package:recipe/values/shared_preferences_keys.dart';
import 'package:recipe/values/strings.dart';
import 'package:recipe/widgets/ingredient_card.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';
import 'package:recipe/widgets/show_bottom.dart';
import 'package:recipe/widgets/show_top.dart';

import 'home_page.dart';
import 'main_page.dart';

class ProfilePage extends StatefulWidget {
  static const String route = 'profile';

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final IngredientsService ingredientsService = IngredientsService();

  Future<BannedIngredients> _banned;
  int delayAmount = 500;

  @override
  void initState() {
    super.initState();
    refreshBanned();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
          onPressed: () {
            Navigator.pushNamed(context, MainPage.route);
          },
          icon: Icon(Icons.arrow_back_rounded),
          color: Colors.white,
        ),
        backgroundColor: Colors.amber,
        elevation: 0.0,
        title: Text('MYRECIPE',
            style: TextStyle(
                fontSize: 18.0,
                letterSpacing: 4.0,
                fontWeight: FontWeight.w400,
                color: Colors.white)),
        centerTitle: true,
      ),
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient (
                    colors: [
                      Colors.amber,
                      Colors.amberAccent,
                    ],
                    begin: const FractionalOffset(0.0, 0.6),
                    end: const FractionalOffset(0, 0),
                    stops: [0.0, 1.0],
                    tileMode: TileMode.clamp
                )
            ),
          ),
          TopAmberCurve(),
          Container(
            padding: const EdgeInsets.all(40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ShowBottom(child: Text(
                  "Welcome\nto your member area",
                  style: title,
                ), delay: delayAmount,
                ),
                SizedBox(height: 15.0),
                ShowBottom(child: Text(
                  "Find the summary of your shopping list and your ingredients banned from your diet",
                  style: subtitle,
                ), delay: delayAmount + 400,
                ),
                SizedBox(height: 32.0),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(
                        Icons.rule_rounded,
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      Strings.BANNED_INGREDIENTS.toUpperCase(),
                      style: subtitle,
                    ),
                  ],
                ),
                FutureBuilder(
                    future: _banned,
                    builder: (ctx, snap) {
                      if (null == snap || !snap.hasData) {
                        return CircularProgressIndicator(
                          valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                        );
                      }

                      final BannedIngredients data = snap.data;
                      if (data.banned.isEmpty) {
                        return Text("No ingredients banned");
                      }

                      return Wrap(
                        children: buildIngredientCardsArray(context, data.banned),
                      );
                    }),
                SizedBox(height: 32.0),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(
                        Icons.storefront_rounded,
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      Strings.INGREDIENTS_LIST.toUpperCase(),
                      style: subtitle,
                    ),
                  ],
                ),
                FutureBuilder(
                  future: StreamingSharedPreferences.instance,
                  builder: (ctx, snap) {
                    if(snap == null || !snap.hasData){
                      return CircularProgressIndicator(
                        valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                      );
                    }
                    final StreamingSharedPreferences sp = snap.data;

                    return PreferenceBuilder(
                      preference: sp.getStringList(
                          SharedPreferencesKeys.INGREDIENTS_LIST,
                          defaultValue: List<String>()),
                      builder: (ctx, List<String> ingredientsIds){
                        return Wrap(
                          children: buildIngredientCardsArrayIds(context, ingredientsIds),
                        );
                      },
                    );
                  },
                ),

                SizedBox(height: 32.0),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.amberAccent.withOpacity(0.6),
                        spreadRadius: 8,
                        blurRadius: 13,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  child: FlatButton.icon(
                    padding: EdgeInsets.only(
                        left: 30, right: 30, top: 15, bottom: 15),
                    color: Colors.amberAccent,
                    onPressed: () async {
                      final sp = await StreamingSharedPreferences.instance;
                      sp.setString(SharedPreferencesKeys.USER_NICKNAME, "");
                      _navigateToHome(context);
                    },
                    icon: Text(
                      "Log out",
                      style: TextStyle(
                          fontFamily: 'Quicksand',
                          fontWeight: FontWeight.w700,
                          fontSize: 18.0,
                          color: Colors.white),
                    ),
                    label: Icon(
                      Icons.logout,
                      color: Colors.white,
                      size: 24.0,
                    ),
                    shape: RoundedRectangleBorder(
                        side: BorderSide(
                            color: Colors.white,
                            width: 2,
                            style: BorderStyle.solid),
                        borderRadius: BorderRadius.circular(30)),
                  ),
                ),
                SizedBox(height: 15.0),
                Text(
                  "Thank you for using MyRecipe, see you soon !",
                  style: textBlack,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> buildIngredientCardsArrayIds(final BuildContext context, List<String> ingredientIds) {
    var results = List<Widget>();
    for(var igdId in ingredientIds){
      final FutureBuilder<Ingredient> item = FutureBuilder(
          future: ingredientsService.getOne(igdId),
          builder: (ctx, snap){
            if(null == snap || !snap.hasData){
              return CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
              );
            }
            final Ingredient ingredient = snap.data;
            return IngredientCard(context: context, ingredient: ingredient);
          }
      );

      results.add(item);
    }

    return results;
  }

  List<Widget> buildIngredientCardsArray(
      final BuildContext context, List<Ingredient> ingredients) {
    var results = List<Widget>();
    for (var igd in ingredients) {
      results.add(IngredientCard(
        context: context,
        ingredient: igd,
        banned: true,
        afterCloseModal: refreshBanned,
      ));
    }
    return results;
  }

  void refreshBanned() {
    setState(() {
      this._banned = ingredientsService.banned();
    });
  }

  void _navigateToHome(BuildContext context) {
    Navigator.pushNamed(context, HomePage.route);
  }
}