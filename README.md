<div align="center">
  <a href="#">
    <img alt="Open Source" src="https://badgen.net/badge/Open%20Source/Yes/green?icon=github" />
  </a>
  <a href="#">
    <img alt="Language" src="https://img.shields.io/badge/Language-Dart-blue.svg" />
  </a>
  <a href="#">
    <img alt="Status" src="https://img.shields.io/badge/Status-Unreleased-red.svg" />
  </a>
</div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <div>
    <a href="https://gitlab.com/7xanswer/myrecipe">
      <img src="images/ic_launcher.png" alt="Logo" width="80" height="80">
    </a>
  </div>
  <h3>MyRecipe</h3>
  <p>
    A simple and intuitive application to guide you in the preparation of your meals.
    <br />
    <a href="https://gitlab.com/7xanswer/myrecipe/-/tree/master/lib"><strong>Sources »</strong></a>
    <br />
    <a href="https://gitlab.com/7xanswer/myrecipe/-/project_members">Project members</a>
    ·
    <a href="https://gitlab.com/7xanswer/myrecipe">Repository</a>
    ·
    <a href="https://gitlab.com/7xanswer/myrecipe/-/issues">Report bug</a>
  </p>
  <p>
    Demo view
    <br />
    <a href="https://youtu.be/EiYTx6SkxJs">YouTube</a>
  </p>
</div>

<!-- ABOUT -->

## About

MyRecipe is an recipe application made with Flutter.

The application offers many features.

- Find new ideas for lunch and dinner
- Ingredients needed for recipes
- Food preferences, shopping list, recipe search and more

<p align="center" width="100%">
    <img width="32%" src="images/1.jpg">
    <img width="32%" src="images/2.jpg">
    <img width="32%" src="images/3.jpg">
</p>

<!-- FEATURES -->

## Features

Intuitive and modern interfaces to manage recipes

`Main` : refresh the page to discover a new dish.

`Menu` : plan a recipe in your week's schedule.

`Profile` : Summary of your food preferences and shopping list.

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place. Any contributions you make are **greatly appreciated**.

Fork the project

Create your feature branch :

```sh
git checkout -b feature/MyFeature
```

Commit your changes :

```sh
git commit -m 'add MyFeature'
```

Push to the branch :

```sh
git push origin 'feature/MyFeature`
```

Open a pull request

<!-- CONTACT -->

## Contact

<p>
    Théo Hélary : theo.helary.59@gmail.com
    <br />
    Isaac Ruchalski : isaac.ruchalski@gmail.com
    <br />
    Boubacar Hama Daouda : boubacarhamadaouda@gmail.com
    <br />
    Guillaume Le Louarn : 7guillaumelelouarn@gmail.com
</p>
